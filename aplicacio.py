from menu import Menu
import crear_taula
import psycopg2
from llibreta import Llibreta


class Aplicacio:
    def __init__(self):
        self.conn = psycopg2.connect("dbname='db' user='iam46420070' host='localhost' password='danielm10'")
        self.menu = Menu()
        self.llibreta = Llibreta(self.conn)

    def inici(self):
        crear_taula.crear_taula(self.conn)
        while True:
            self.menu.mostrar_menu_principal()
            opcio = int(input("Selecciona una opcio valida: "))
            if opcio == 5:
                break
            self.menus(opcio)

    def menus(self, opcio):
        if opcio == 1:
            nom = input("Quin es el nom del client: ")
            cognom = input("Quin es el cognom del client: ")
            telefon = input("Quin es el telefon del client: ")
            correu = input("Quin es el correu del client: ")
            adreca = input("Quina es l'adreca del client: ")
            ciutat = input("De quina ciutat es el client: ")

            self.llibreta.afegir_client(nom, cognom, telefon, correu, adreca, ciutat)

        elif opcio == 2:
            self.llibreta.get_llista_clients()
            id = int(input("Quin es l'identificador del client que vols eliminar? "))
            self.llibreta.eliminar_client(id)

        elif opcio == 3:
            self.menu.mostrar_menu_consulta()
            opcio_consulta = int(input("Selecciona una opcio valida: "))

            while True:

                if opcio_consulta == 1:
                    id = int(input("Quin es l'identificador del client que vols buscar? "))
                    llista_clients = self.llibreta.cercar_per_id(id)
                    print(llista_clients)

                elif opcio_consulta == 2:
                    nom = input("Quin es el nom del client que vols buscar? ")
                    llista_clients = self.llibreta.cerca_per_nom(nom)
                    for client in llista_clients:
                        print(client)

                elif opcio_consulta == 3:
                    cognom = input("Quin es el cognom del client que vols buscar? ")
                    llista_clients = self.llibreta.cerca_per_cognom(cognom)
                    for client in llista_clients:
                        print(client)

                elif opcio_consulta == 4:
                    self.llibreta.get_llista_clients()

                elif opcio_consulta == 5:
                    self.llibreta.get_llista_clients_ordenada()

                elif opcio_consulta == 6:
                    break

                self.menu.mostrar_menu_consulta()
                opcio_consulta = int(input("Selecciona una opcio: "))

        elif opcio == 4:
            self.llibreta.get_llista_clients()
            id = int(input("Quin es el id del client que vols modificar? "))
            print("""
                    =========================
                    Quin camp vols modificar?
                    =========================
                    1. Nom
                    2. Cognom
                    3. Telefon
                    4. Correu
                    5. Adreça
                    6. Ciutat""")
            opcio_modificar = int(input())

            self.llibreta.modificar_camp_client(id, opcio_modificar)


if __name__ == "__main__":
    app = Aplicacio()
    app.inici()
