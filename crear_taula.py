import psycopg2
import sys


def crear_taula(conn):
    try:
        cur = conn.cursor()
        cur.execute("DROP TABLE IF EXISTS Clients;")
        cur.execute("CREATE TABLE IF NOT EXISTS Clients(id_client SERIAL PRIMARY KEY, nom VARCHAR, cognom VARCHAR, telefon VARCHAR, correu VARCHAR, adreca VARCHAR, ciutat VARCHAR);")

        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        if conn is not None:
            conn.rollback()
            print("--Error {0}".format(error))
            sys.exit(1)
