from client import Client
import psycopg2


class Llibreta:

    def __init__(self, conn):
        self.conn = conn
        self.id_client = 0

    def get_llista_clients(self):
        try:
            cursor = self.conn.cursor()
            cursor.execute("SELECT * from Clients;")
            rows = cursor.fetchall()

            for row in rows:
                print(row)

        except (Exception, psycopg2.DatabaseError) as error:
            self.conn.rollback()
            print(error)

    def afegir_client(self, nom, cognom, telefon, correu, adreca, ciutat):
        try:
            cursor = self.conn.cursor()
            cursor.execute(
                f"INSERT INTO Clients (nom, cognom, telefon, correu, adreca, ciutat) VALUES ('{nom}', '{cognom}', '{telefon}', '{correu}', '{adreca}', '{ciutat}');")
            result = cursor.statusmessage
            self.conn.commit()

        except (Exception, psycopg2.DatabaseError) as error:
            self.conn.rollback()
            result = error
        print(result)

    def eliminar_client(self, id_client):
        try:
            cursor = self.conn.cursor()
            cursor.execute(f"DELETE FROM Clients WHERE id_client = {id_client};")
            result = cursor.statusmessage
            self.conn.commit()

        except (Exception, psycopg2.DatabaseError) as error:
            self.conn.rollback()
            result = error
        print(result)

    def cercar_per_id(self, id_client):
        try:
            llista_clients = []
            cursor = self.conn.cursor()
            cursor.execute(f"SELECT * FROM Clients WHERE id_client = {id_client};")
            rows = cursor.fetchall()

            for row in rows:
                llista_clients.append(row)

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

        return llista_clients

    def cerca_per_nom(self, nom):
        try:
            llista_clients = []
            cursor = self.conn.cursor()
            cursor.execute(f"SELECT * FROM Clients WHERE nom = '{nom}';")

            rows = cursor.fetchall()

            for row in rows:
                llista_clients.append(row)

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

        return llista_clients

    def cerca_per_cognom(self, cognom):
        try:
            llista_clients = []
            cursor = self.conn.cursor()
            cursor.execute(f"SELECT * FROM Clients WHERE cognom = '{cognom}';")

            rows = cursor.fetchall()

            for row in rows:
                llista_clients.append(row)

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

        return llista_clients

    def get_llista_clients_ordenada(self):
        try:
            llista_clients = []
            cursor = self.conn.cursor()
            cursor.execute("SELECT * from Clients ORDER BY nom;")
            rows = cursor.fetchall()

            for row in rows:
                print(row)

        except (Exception, psycopg2.DatabaseError) as error:
            print(error)

    def modificar_camp_client(self, id, opcio):
        if opcio == 1:
            nom = input("Quin es el nou nom que li vols donar? ")
            try:
                cursor = self.conn.cursor()
                cursor.execute(f"UPDATE Clients SET nom = '{nom}' WHERE id_client = '{id}'")
                result = cursor.statusmessage
                self.conn.commit()
            except (Exception, psycopg2.DatabaseError) as error:
                self.conn.rollback()
                result = error
            print(result)

        elif opcio == 2:
            cognom = input("Quin es el nou cognom que li vols donar? ")
            try:
                cursor = self.conn.cursor()
                cursor.execute(f"UPDATE Clients SET cognom = '{cognom}' WHERE id_client = '{id}'")
                result = cursor.statusmessage
                self.conn.commit()

            except (Exception, psycopg2.DatabaseError) as error:
                self.conn.rollback()
                result = error
            print(result)

        elif opcio == 3:
            telefon = input("Quin es el nou telefon que li vols donar? ")
            try:
                cursor = self.conn.cursor()
                cursor.execute(f"UPDATE Clients SET telefon = '{telefon}' WHERE id_client = '{id}'")
                result = cursor.statusmessage
                self.conn.commit()

            except (Exception, psycopg2.DatabaseError) as error:
                self.conn.rollback()
                result = error
            print(result)

        elif opcio == 4:
            correu = input("Quina es el nou correu que li vols donar? ")
            try:
                cursor = self.conn.cursor()
                cursor.execute(f"UPDATE Clients SET adreca = '{correu}' WHERE id_client = '{id}'")
                result = cursor.statusmessage
                self.conn.commit()

            except (Exception, psycopg2.DatabaseError) as error:
                self.conn.rollback()
                result = error
            print(result)

        elif opcio == 5:
            adreca = input("Quina es la nova adreca que li vols donar? ")
            try:
                cursor = self.conn.cursor()
                cursor.execute(f"UPDATE Clients SET adreca = '{adreca}' WHERE id_client = '{id}'")
                result = cursor.statusmessage
                self.conn.commit()

            except (Exception, psycopg2.DatabaseError) as error:
                self.conn.rollback()
                result = error
            print(result)

        elif opcio == 6:
            ciutat = input("Quina es la nova ciutat que li vols donar? ")
            try:
                cursor = self.conn.cursor()
                cursor.execute(f"UPDATE Clients SET ciutat = '{ciutat}' WHERE id_client = '{id}'")
                result = cursor.statusmessage
                self.conn.commit()

            except (Exception, psycopg2.DatabaseError) as error:
                self.conn.rollback()
                result = error
            print(result)
        else:
            print("Introdueix una opció correcta: ")
