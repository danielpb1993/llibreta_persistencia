class Menu:
    def mostrar_menu_principal(self):
        print("""
                MENU PRINCIPAL
                ==================================
                Seleccioni una opcio i premi Intro
                ==================================
                1. Afegir Client
                2. Eliminar Client
                3. Consultar Client
                4. Modificar un camp d'un client (*)
                5. Sortir
            """)

    def mostrar_menu_consulta(self):
        print("""
                MENU CONSULTA
                ==================================
                Seleccioni una opcio i premi Intro
                ==================================
                1. Cercar client per identificador
                2. Cercar client per nom
                3. Cercar client per cognom
                4. Llistar tots els clients
                5. Llistar tots els clients per Nom
                6. Enrere
            """)
